/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.TokoNew;
import java.io.Serializable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface TokoNewDao extends PagingAndSortingRepository<TokoNew, String>{
    
    @Query("SELECT COUNT(id) FROM TokoNew WHERE agent =  ?1")
    public int findByAgentId(String id_user);
    
}
