/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.AgentNew;
import java.io.Serializable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface AgentNewDao extends  PagingAndSortingRepository<AgentNew, String>{
    
    @Query("SELECT COUNT(id) FROM AgentNew WHERE uplineid = ?1")
    public int findByUplineId(String id_user);
    
}
