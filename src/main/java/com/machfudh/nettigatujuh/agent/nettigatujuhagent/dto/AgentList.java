/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity
@Table(name="net_vlistfollowagent")
public class AgentList implements Serializable{
    
  @Id   
  private String id;
  private String nama;
  private String alamat;
  private String uplineid;
  private String thumbnail;
  private String medium;
  private String original;
  private String pesan;
    
}
