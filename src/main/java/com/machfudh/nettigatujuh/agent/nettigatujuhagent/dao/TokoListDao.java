/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.TokoList;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface TokoListDao extends PagingAndSortingRepository<TokoList, String>{
    
    public List<TokoList> findByAgent(String id_user);
    
    
            
    
    
    
}
