/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.Toko;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface TokoDao extends PagingAndSortingRepository<Toko, String>{

//  //mendapatkan  total toko langsung - segaris 
//    @Query("SELECT COUNT(t.id) FROM net_toko t WHERE t.id_agent = ?1 ")
//    public int findById_agent(String id_agent);
//    
//    //mendapatkan list new toko yg blom approve
//    @Query("SELECT a.* FROM net_vlistfollowtoko a WHERE a.id = ?1")
//    public List<TokoList> findFollowToko(String id_user);
    
}

