/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.controller;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.AgentDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.AgentListDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.AgentNewDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.TokoDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.TokoListDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao.TokoNewDao;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.AgentList;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.RestAccount;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.TokoList;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.entity.Agent;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Moh Machfudh
 */
@RestController
public class AgentController {

    public static final Logger LOGGER = LoggerFactory.getLogger(AgentController.class);

    @Autowired
    private AgentDao agentDao;
    
    @Autowired
    private TokoDao tokoDao;
    
    @Autowired
    private AgentListDao agentListDao;
    
    @Autowired
    private AgentNewDao agentNewDao;
    
    @Autowired
    private TokoListDao tokoListDao;
    
    @Autowired
    private TokoNewDao tokoNewDao;

    private Date tglsnow = new Date();

    @PreAuthorize("hasAuthority('AGENT_VIEW')") 
    @GetMapping("/api/agent")
    @ResponseBody
    public Page<Agent> dataAgent(Pageable page) {
        return agentDao.findAll(page);
    }
    
    @PreAuthorize("hasAuthority('AGENT_EDIT')")
    @PostMapping("/api/agent")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveAgent(@RequestBody @Valid Agent agent) {
        //cek email
        if(agentDao.findByEmail(agent.getEmail()) != null){
           String pesan = "Email sudah terdaftar ";
           LOGGER.error("====================== Emai : ",pesan);
           return;
        }
        tglsnow = new Date();        
        agent.setInsertdate(Long.toString(tglsnow.getTime()));
        agent.setEditdate(Long.toString(tglsnow.getTime()));
        agent.setNoanggota("AG" + Long.toString(tglsnow.getTime()));
        agentDao.save(agent);
    }

    @PreAuthorize("hasAuthority('AGENT_EDIT')")
    @PutMapping("api/agent/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editAgent(@PathVariable(name = "id") String id, @RequestBody @Valid Agent agent) {
        Agent newAgent = agentDao.findOne(id);

        if (newAgent == null) {
            return;
        }
        tglsnow = new Date();
        BeanUtils.copyProperties(agent, newAgent);
        newAgent.setId(id);
        newAgent.setEditdate(Long.toString(tglsnow.getTime()));
        agentDao.save(newAgent);
    }

    @PreAuthorize("hasAuthority('AGENT_VIEW')")
    @GetMapping("/api/agent/{id}")
    @ResponseBody
    public Agent agenById(@PathVariable(name = "id") String id) {
        Agent agent = agentDao.findOne(id);
        return agent;
    }
    
    @PreAuthorize("hasAuthority('AGENT_VIEW')")
    @GetMapping("/api/agent/user/{iduser}")
    @ResponseBody
    public RestAccount agenByIdUser(@PathVariable(name = "iduser") String iduser) {
        
        RestAccount account = new RestAccount();
        Agent agent = agentDao.findByUser(iduser);
        int jbonus = 10000;
        account.setStatus("200");
        account.setAgent(agentNewDao.findByUplineId(iduser));
        account.setToko(tokoNewDao.findByAgentId(iduser));
        account.setBonus(jbonus);
        account.setJagent(agent);
        account.setListagent(agentListDao.findByUplineid(iduser));
        account.setListtoko(tokoListDao.findByAgent(iduser));
        return account;
    }
    
    
}
