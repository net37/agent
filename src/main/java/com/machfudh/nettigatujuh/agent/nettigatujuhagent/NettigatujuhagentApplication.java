package com.machfudh.nettigatujuh.agent.nettigatujuhagent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
//@EnableSwagger2
public class NettigatujuhagentApplication {

	public static void main(String[] args) {
		SpringApplication.run(NettigatujuhagentApplication.class, args);
	}
        

}
