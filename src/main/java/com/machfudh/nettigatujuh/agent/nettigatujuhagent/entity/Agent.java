/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity @Table( name = "net_agent")
public class Agent implements Serializable{
    
    private static final long serialVersionUID = -7371610187321351709L;
    
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2") 
    private String id;
    
    @NotNull @NotEmpty @Column(name = "id_user")
    private String user;
    
    @NotNull @NotEmpty
    private String panggil;
    
    @NotNull @NotEmpty
    private String nama;
    
    @NotNull @NotEmpty
    private String jeniskel;
    
    @NotNull @NotEmpty
    private String tgllahir;
    
    @NotNull @NotEmpty
    private String nohp;
    
    @NotNull @NotEmpty @Column( unique = true ) 
    private String email;
    
    @NotNull @NotEmpty
    private String alamat;
    
    @NotNull @NotEmpty @Size( max = 5 )
    private String kodepos;
    
    @NotNull @NotEmpty
    private String noktp;
    
    private String nonpwp;
    private String norekening;
    private String photodiri;
    private String photoktp;
    
    @NotNull @NotEmpty    
    private String uplineid;
    
    private String noanggota;
    
    @NotNull @NotEmpty
    private String inserid;
    
    @NotNull @NotEmpty
    private String insertdate;
    
    private String editid;
    private String editdate;
    private boolean active = true;
    private String status;
    
}
