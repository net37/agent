/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity
@Table(name = "net_toko")
public class Toko {
    
    @Id 
    private String id; 
    
    private String namatoko;
    private String keterangan;
    private String modeltoko;
    private String buka;
    private String tutup;
    private String statusbuka;
    private String phototoko;
    private String alamattoko;
    private String kodepostoko;
    private String pemilik;
    private String nohp;
    private String email;
    private String alamat;
    private String kodepos;
    private String noktp;
    private String nonpwp;
    private String noaccount;
    private String photopemilik; 
    private String photoktp;
    private String status;
    private String membership;
    private String catatan;
    private String longtitut;
    private String latitut;
    private String lastlogin;
    private String id_agent;
    private String insertid;
    private String insertdate;
    private String editid;
    private String editdate;
    private String id_user;
    private String notoko;
    
}
