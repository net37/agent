/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dao;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto.AgentList;
import com.machfudh.nettigatujuh.agent.nettigatujuhagent.entity.Agent;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface AgentDao extends PagingAndSortingRepository<Agent, String>{
    
    public Agent findByEmail(String email);
        
    
    public Agent findByUser(String id_user);
    
    
    
    
//    public List<AgentList> findByUpline(String id_user);
    
//    //mendapatkan  total toko langsung - segaris 
//    @Query("SELECT COUNT(id) FROM net_agent")
//    public int totalAgen();
//    
//    //mendapatkan list new agent yg blom approve
//    @Query("SELECT a.* FROM net_vlistfollowagent a WHERE a.id = ?1")
//    public List<AgentList> findFollowAgent(String id_user);
    
}
