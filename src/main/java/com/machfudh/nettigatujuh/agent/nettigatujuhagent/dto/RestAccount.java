/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.agent.nettigatujuhagent.dto;

import com.machfudh.nettigatujuh.agent.nettigatujuhagent.entity.Agent;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Moh Machfudh
 */
@Data
public class RestAccount {
    
    private String status;
    private int toko;
    private int agent;
    private int bonus;
    private Agent jagent;
    private List<AgentList> listagent;
    private List<TokoList> listtoko;
    
    
}
